from random import randint
import argparse
import re

class BuildGenerator:

    def __init__(self, items):
        self.items = items
        self.excludeCategories = ['Consumables', 'Roshan Drop', 'Common', 'Attributes', 'Secret', 'Armaments', 'Arcane']
        self.boots = ['Boots of bearing', 'Guardian Greaves', 'Power Treads', 'Phase Boots', 'Arcane Boots', 'Tranquil Boots', 'Boots of Travel', 'Boots of Speed']
        self.totalPrice = 0
        self.build = ''

    def createBuild(self, numberOfItems):
        categories = [category for category in self.items if category not in self.excludeCategories]
        selectedBoot = self.boots[randint(0, len(self.boots[:-1]) - 1)]
        bootCost = self.findBootPrice(selectedBoot)
        self.updateTotalCost(bootCost)
        self.addItem(selectedBoot + ' ({amt})'.format(amt=bootCost))
        print(selectedBoot + ' ({amt})'.format(amt=bootCost))

        self.removeBootsFromItems()

        for i in range(0, numberOfItems):
            randomCategory = categories[randint(0, len(categories) - 1)]
            items = self.items[randomCategory]
            item = items.pop(randint(0, len(items) - 1))
            self.updateTotalCost(self.getItemCost(item))
            self.addItem(item)
            print(item)

        self.addItem("--------------------------")
        self.addItem("Total cost: " + str(self.totalPrice))
        print("--------------------------")
        print("Total cost: " + str(self.totalPrice))

    def findBootPrice(self, boot):
        for categoryList in self.items:
            l = [item for item in self.items[categoryList] if boot in item]
            if len(l) > 0:
                return self.getItemCost(l[0])

    def getItems(self):
        return self.items

    def removeBootsFromItems(self):
        for boot in self.boots:
            for categoryList in self.items:
                self.items[categoryList] = list(filter(lambda s: not (s.startswith(boot)), self.items[categoryList]))

    def updateTotalCost(self, amount):
#         print('')
        self.totalPrice = self.totalPrice + amount

    def getItemCost(self, item):
        price = re.search(r"\((\d+)\)", item)
        if price is not None:
            return int(price.group(1))

    def addItem(self, line):
        self.build = self.build + line + '\n'

    def getBuild(self):
        return self.build