import os

import discord
from dotenv import load_dotenv
import random

from BuildGeneratorV3 import BuildGenerator
from FileHandlerV3 import FileHandler

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print(f'{client.user.name} has connected to Discord!')

@client.event
async def on_member_join(member):
    await member.create_dm()
    await member.dm_channel.send(
        f'Hi {member.name}, welcome to my Discord server!'
    )

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('give me courage'):
        buildGenerator = BuildGenerator(FileHandler('config').read())
        buildGenerator.createBuild(5)
        response = message.author.name + '\n' + '--------------------------' + '\n' + buildGenerator.getBuild()
        await message.channel.send(response)

client.run(TOKEN)