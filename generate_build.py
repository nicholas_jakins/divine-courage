from BuildGeneratorV3 import BuildGenerator
from FileHandlerV3 import FileHandler
import argparse



parser = argparse.ArgumentParser(description='List of people.')

# Add a positional argument that accepts a list of strings
parser.add_argument('string_list', nargs='+', type=str, help='List of names')

# Parse the command-line arguments
args = parser.parse_args()

# Access the list of strings
string_list = args.string_list

# Your script logic with the parameter string_list
for name in string_list:
    print('=' * 20)
    print('\t' * 1 + name)
    print('=' * 20)
    buildGenerator = BuildGenerator(FileHandler('config').read())
    buildGenerator.createBuild(5)
    print('\n')
