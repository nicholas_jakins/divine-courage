# Divine Courage - Discord Bot

A personal favourite MOBA of mine is Dota 2. A specific variant of the game to 
be played with friends is when you generate and stick to a random item build.  

Implemented as a discord bot, this project allows chat participants 
to generate random item builds for use when playing Dota 2.

1. Current and up-to-date items are scraped from the fan [Wiki](https://dota2.fandom.com/wiki/Item_Grid) and written to a JSON formatted file. This can be done ahead of time. `python FileHandler3.py`
2. The bot needs to be added to a guild using the [Discord developer Portal](https://discord.com/developers/applications).
3. The bot token needs to be added to the .env file.
4. Run the bot `python bot.py`

![output.gif](demo.gif)