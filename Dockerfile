# Use the official Python image as a base image
FROM python:3.9-slim

# Set the working directory inside the container
WORKDIR /usr/src/app

# Copy the bot files into the container
COPY . .

# Install bot dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Command to run the bot when the container starts
CMD ["python", "bot.py"]
