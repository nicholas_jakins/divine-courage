from bs4 import BeautifulSoup
import urllib.request

class ContentRetriever:
    
    def __init__(self):
        self.GET_URL = 'https://dota2.fandom.com/wiki/Item_Grid'
    
    def getPageContents(self):
        webUrl = urllib.request.urlopen(self.GET_URL)
        return webUrl.read() 
    
class ContentParser:
    
    def __init__(self, retriever):
        self.CONTENTS = retriever.getPageContents()
        self.TITLE = "title"
        self.A = "a"
        self.LI = "li"
        self.TD = "td"
        self.TABLE = "table"
        self.TR = "tr"
        
    def getRawPage(self):
        return self.CONTENTS

    def extractTitle(self, tableEntry):
        a = tableEntry.find(self.A)        
        return a[self.TITLE]
        
    def extractColumns(self, tableRow):
        return tableRow.find_all(self.TD)
    
    def extractItemsFromCategory(self, li):
        return [l.find(self.A)[self.TITLE] for l in li]        
    
    def getItems(self):
        soup = BeautifulSoup(self.getRawPage(), 'html.parser')
        table = soup.find(self.TABLE)
        
        rows = table.find_all(self.TR)
        categories = [self.extractTitle(tableEntry) for tableEntry in self.extractColumns(rows[0])]
        
        items = []
        
        for row in rows[1:]:
            columns = self.extractColumns(row)
            for td in columns:
                li = td.find_all(self.LI)
                items.append(self.extractItemsFromCategory(li))
        
        return dict(zip(categories, items))