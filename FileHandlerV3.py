import json
from os import listdir
from os.path import isfile, join

class FileHandler:

    def __init__(self, directory):
        self.FILES = [join(directory, f) for f in listdir(directory) if isfile(join(directory, f))]

    def write(self, filename, contents):
        with open(filename, 'w') as fp:
            json.dump(contents, fp)
        
    def read(self):
        items = {}
        for file in self.FILES:
            with open(file, 'r') as f:
                items = {**items, **json.loads(f.read())}
        return items